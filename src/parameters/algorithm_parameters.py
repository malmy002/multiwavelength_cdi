"""
Class for CDI algorithm parameters.
"""

class Alg_Par(object):
    def __init__(self,
            beta=0.8,
            ep=1.0,
            er_it=100,
            hio_it=50,
            error_freq=5,
            S_constraint=1):

        """
        beta: HIO parameter
        ep: regularization for MWCDI
        er_it: ER number of iterations (in ERHIO)
        hio_it: number of HIO iterations (in ERHIO)
        period: er_it + hio_it
        S_constraint: [0,1]
            1: full constraint
            0: fully relaxed
            ui = ui((1-S_constraint) + (S_constraint) * sqrt(||I||)/||u||)
        """
        # Calculate Error every ___ iterations.
        self.error_freq = error_freq

        # HIO
        self.beta = beta

        # ERHIO
        self.er_it = er_it
        self.hio_it = hio_it
        self.period = er_it + hio_it

        #Multi-wavelength
        self.ep = ep
        self.S_constraint = S_constraint
