"""
General support parameters
"""


class Sup_Par(object):
    def __init__(self, sup_freq=20, update_during='hio'):

        """ How often the support is updated """
        self.sup_freq = sup_freq

        """When to update (during 'ER' or 'HIO' or 'both')"""
        self.update_during = update_during
