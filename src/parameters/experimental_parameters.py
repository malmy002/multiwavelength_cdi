"""
Class for experimental parameters.
"""

import numpy as np


class Exp_Par(object):
    def __init__(self, z=None, lamb=None, pix0=None, pix2=None,
        spec_int=None, regime='far'):

        self.z = z
        self.lamb = lamb
        self.pix0 = pix0
        self.pix2 = pix2
        if spec_int is not None:
            self.spec_int = np.array(spec_int)/np.sum(spec_int)
        else:
            self.spec_int = None
        self.regime = regime
