"""
Class for multiwavelength CDI (base class)
"""

from numpy.fft import fft2, ifft2, fftshift
import numpy as np
from .cdi import CDI


class MWCDI(CDI):
    """ Base class for the multi-wavelength CDI classes.
    """
    def __init__(self, g, support_parameters, algorithm_parameters,
        experiment_parameters, sup=None, mask=None):
        """
        g: Fourier data  (fftshifted into the corners)
        sup: initial support
        mask: Fourier mask: 1: measured data, 0: no data (bad data)

        """
        # Number of wavelengths
        self.nlamb = len(experiment_parameters.lamb)
        
        # Use max(S) for updates
        self.u_idx = np.argmax(experiment_parameters.spec_int)

        CDI.__init__(self, g,
        support_parameters, algorithm_parameters,
        experiment_parameters, sup=sup, mask=mask)

        # Initialize support (returns sup)
        self.init_support(sup)

        # Initialize exit surface wave
        self.u = self.init_esw()
        self.u_prev = self.u.copy()

        # For spectral constraint
        self.Itot = np.linalg.norm(ifft2(self.g), ord=2)
        self.gnorm = np.linalg.norm(self.g, 2)

        self.spec_int = experiment_parameters.spec_int


    def propagate(self):
        """ Propagate to Fourier-space.
        """
        self.u = fft2(self.u, axes=(1,2))


    def back_propagate(self):
        """ Back-propagate to sample-plane.
        """
        self.u = ifft2(self.u, axes=(1,2))


    def init_esw(self):
        """
        u0.shape: (P, N, N), where P: number of wavelengths
        """
        rnd = np.random.rand(self.g.size*self.nlamb).reshape((self.nlamb,
        self.sh[0],self.sh[1]))
        u0 = np.einsum('ijk,ijk->ijk', rnd, self.sup).astype('complex')
        return u0


    def init_support(self, sup0):
        """ Initialize support
        """
        self.sup = sup0.copy()


    def fourier_space_projections1(self):
        """ MW Fourier update:
        """
        self.u[:,self.mask] =  self.g[self.mask] * \
            self.u[:,self.mask] / (np.sqrt(
                self._calc_I(self.u, self.spec_int))[self.mask] + self.ap.ep)


    def real_space_projections1(self):
        self.u = self.u * self.sup


    def real_space_projections2(self):
        """ Impose spectral intensity constraints.
        No need to account for pixel size differences:  fft doesn't account for this.
        |fft(u)| will be the same.
        """
        norms = np.linalg.norm(self.u, axis=(-2,-1))
        self.u = self.ap.S_constraint*np.einsum('ijk,i->ijk',
        self.Itot*self.u, 1.0/norms) + (1.0-self.ap.S_constraint) * self.u


    def _calc_I(self, u, S=None):
        """ Calculate incoherent sum.
        """
        if S is None:
            return np.sum(np.square(u), axis=0)
        else:
            return np.einsum('ijk, i -> jk', np.square(np.abs(u)), S)
