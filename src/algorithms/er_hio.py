"""
Class for ERHIO.
"""

from .cdi import CDI
import numpy as np
from numpy.fft import fft2, ifft2, fftshift


class ERHIO(CDI):
    def __init__(self, g, support_parameters, algorithm_parameters,
        experiment_parameters, sup=None, mask=None):

        # Counter for switching between ER & HIO
        self.counter = 0

        CDI.__init__(self, g, support_parameters,
            algorithm_parameters, experiment_parameters, sup=sup, mask=mask)

        # To switch shrinkwrap on or off
        try:
            self.sup_freq = self.sp.sup_freq
        except:
            self.sup_freq = 100

    def real_space_projections1(self):

        # During ER: self.sp.sup_freq -> ~10000
        # er_it (100), hio_it (50)

        if self.it%self.ap.period < self.ap.hio_it:
            self.ER = 0
        else:
            self.ER = 1

        if np.mod(self.it+1, self.sup_freq) == 0:
            if 'hio' in self.sp.update_during.lower() and self.ER == 1:
                self.sp.sup_freq = 100000
            elif 'er' in self.sp.update_during.lower() and self.ER == 0:
                self.sp.sup_freq = 100000
            else:
                self.sp.sup_freq = self.sup_freq

        """ HIO or ER projection """
        if self.ER == 0:
            self.u[~self.sup] = self.u_prev[~self.sup] - \
                self.ap.beta * self.u[~self.sup]
        else:
            self.u[~self.sup] = 0.0

        self.us = self.u.copy()
        self.us[~self.sup] = 0.0
        self.u_prev = self.u.copy()


    def init_esw(self):
        u0 = np.random.rand(self.g.size).reshape(self.g.shape) * self.sup
        self.u_prev = u0.copy()
        return u0


    def calc_fourier_error(self):

        self.f_error.append(np.sqrt(np.sum(
            np.square(np.abs(fft2(self.us))- self.g))) / \
            np.sqrt(np.sum(np.square(self.g))))
