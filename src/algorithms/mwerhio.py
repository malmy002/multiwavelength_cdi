"""
Class for multi-wavelength ERHIO.
"""
from .mwcdi import MWCDI
import numpy as np
from numpy.fft import fft2, ifft2, fftshift


class MWERHIO(MWCDI):
    def __init__(self, g, support_parameters, algorithm_parameters,
        experiment_parameters, sup=None, mask=None):

        MWCDI.__init__(self, g, support_parameters,
            algorithm_parameters, experiment_parameters, sup=sup, mask=mask)

        # To switch shrinkwrap on or off
        try:
            self.sup_freq = self.sp.sup_freq
        except:
            self.sup_freq = 100


    def real_space_projections1(self):
        # During ER: self.sp.sup_freq -> ~10000
        #er_it (100), hio_it (50)

        if self.it%self.ap.period < self.ap.hio_it:
            self.ER = 0
        else:
            self.ER = 1

        if np.mod(self.it+1, self.sup_freq) == 0:
            if 'hio' in self.sp.update_during.lower() and self.ER == 1:
                self.sp.sup_freq = 100000
            elif 'er' in self.sp.update_during.lower() and self.ER == 0:
                self.sp.sup_freq = 100000
            else:
                self.sp.sup_freq = self.sup_freq

        """ HIO or ER projection """
        if self.ER == 0:
            self.u[~self.sup] = self.u_prev[~self.sup] - \
                self.ap.beta * self.u[~self.sup]
        else:
            self.u[~self.sup] = 0.0

        self.u_prev = self.u.copy()
        self.us = self.u.copy()
        self.us[~self.sup] = 0.0
