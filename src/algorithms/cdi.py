"""
Base class for CDI
"""

from numpy.fft import fft2, ifft2, fftshift
import numpy as np


class CDI(object):
    """ Base class for the CDI classes.

    Performs ER algorithm.
    """

    def __init__(self, g, support_parameters, algorithm_parameters,
        experiment_parameters, sup=None, mask=None):
        """
        g: Fourier data  (fftshifted into the corners)
        sup: initial support
        mask: Fourier mask: 1: measured data, 0: no data (bad data)
        proj: other projections (see: projection directory for classes)
        """

        self.sp = support_parameters
        self.ap = algorithm_parameters
        self.ep = experiment_parameters

        # Data  (fftshifted into corners)
        self.g = g.copy()

        # Data shape
        self.sh = self.g.shape

        # Initialize support (returns sup)
        self.init_support(sup)

        # Initialize exit surface wave
        self.u = self.init_esw()

        # Fourier error
        self.f_error = []

        # Iteration counter
        self.it = 0

        # Fourier Mask
        if mask is not None:
            self.mask = mask.copy().astype('bool')
        else:
            self.mask = np.ones(self.sh, dtype='bool')



    def run(self, it_num=10, cycles=None):
        """
        Run phase retrieval

        cycles: number of ERHIO cycles to run.
        proj: projection operator (see: cimax.projections classes)
        """

        if cycles is not None:
            it_num = cycles * (self.ap.period)

        for a0 in range(it_num):

            self.real_space_projections1()
            self.real_space_projections2()

            # Propagate to Fourier-space
            self.propagate()


            # Calculate Fourier Error
            if np.mod(self.it, self.ap.error_freq) == 0:
                self.calc_fourier_error()

            self.fourier_space_projections0()
            self.fourier_space_projections1()
            self.fourier_space_projections2()

            # Back-propagate to sample-plane
            self.back_propagate()

            # Update support
            if np.mod(self.it, self.sp.sup_freq) == 0:
                self.update_support()

            # Update counter
            self.it += 1


    def propagate(self):
        """ Propagate to Fourier-space.
        """
        self.u = fft2(self.u, axes=(-2,-1))


    def back_propagate(self):
        """ Back-propagate to sample-plane.
        """
        self.u = ifft2(self.u, axes=(-2,-1))


    def init_esw(self):
        u0 = np.random.rand(self.g.size).reshape(self.g.shape) * self.sup
        return u0


    def init_support(self, sup0):
        """ Initialize support
        """
        self.sup = sup0.copy()


    def real_space_projections1(self):
        self.u = self.u * self.sup


    def fourier_space_projections1(self):
        self.u[self.mask] = self.g[self.mask] * \
            np.exp(1j*np.angle(self.u[self.mask]))


    def calc_fourier_error(self):
        self.f_error.append(np.sqrt(np.sum(
            np.square(np.abs(self.u)- self.g))) / \
            np.sqrt(np.sum(np.square(self.g))))


    def real_space_projections2(self):
        pass


    def fourier_space_projections0(self):
        pass

    def fourier_space_projections2(self):
        pass

    def update_support(self):
        pass
