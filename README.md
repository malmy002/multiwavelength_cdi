## Multi-wavelength coherent diffractive imaging #
----------------------------------------------------------

This repository contains the code for the article "Multi-wavelength phase retrieval for coherent diffractive imaging".

Optics Letters 46(1), 13-16 (2021):

https://doi.org/10.1364/OL.408452

Arxiv:

https://arxiv.org/abs/2005.03336 


### Overview ###
Python code for performing phase retrieval on multi-wavelength diffraction patterns.


### Example demos ###
- demos/multiwavelength_cdi_demo.py
- notebooks/multiwavelength_cdi_demo.ipynb


### Dependencies ###
- Numpy
- Matplotlib
- Jupyter-notebook (optional)


### Installation (Linux) ###
- Install dependencies
- Add the directory to the computer's path by placing the line: "export PYTHONPATH=/path/:$PYTHONPATH" into the .bashrc file located in the home directory.


