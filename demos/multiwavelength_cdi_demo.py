import numpy as np
from numpy.fft import fft2, ifft2, fftshift

import multiwavelength_cdi as mw
from matplotlib import pylab as plt


"""Load data"""
#Spectral intensities
S = np.load('../data/S.npy')

#Wavelengths
lambs = np.load('../data/lambs.npy')

#Amplitude data
g = np.load('../data/amp_data.npy')

#Supports
sups = np.load('../data/supports.npy')


#Setup algorithm, support and experimental parameters
ap = mw.par.Alg_Par(ep=1e-5,S_constraint=1)
sp = mw.par.Sup_Par()
ep = mw.par.Exp_Par(lamb=lambs,spec_int=S)


#Create Multi-wavelength ERHIO class instance
mwcdi = mw.alg.MWERHIO(g, sp, ap, ep, sup=sups)

#Run for 750 iterations (5 cycles of MWERHIO)
print("Running multi-wavelength phase retrieval...")
mwcdi.run(cycles=5)

#ERHIO (monochromatic)
cdi = mw.alg.ERHIO(g, sp, ap, ep, sup=sups[0,:,:])

#Run algorithm for 750 iterations (5 cycles of ERHIO)
print("Running monochromatic phase retrieval...")
cdi.run(cycles=5)


# Show intensity (log-scale)
plt.figure()
plt.imshow(fftshift(np.log10(np.maximum(g**2,0.5))), cmap='gray_r');
plt.title('Intensity data (log-scale)')

# Monochromatic reconstruction
plt.figure()
plt.imshow(fftshift(np.abs(cdi.u)), cmap='gray');
plt.title('Monochromatic reconstruction');

# Show reconstructions
plt.figure(figsize=(8,4))
plt.subplot(121)
plt.imshow(fftshift(np.abs(mwcdi.u[0,:,:])), cmap='gray')
plt.title('$\lambda_1$ reconstruction');
plt.subplot(122)
plt.imshow(fftshift(np.abs(mwcdi.u[1,:,:])), cmap='gray');
plt.title('$\lambda_2$ reconstruction');


plt.show();
